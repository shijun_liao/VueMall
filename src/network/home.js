//当export 没有写default 时候，都需要添加{}这个符号

import {request} from '@/network/request'

export function getHomeMultidata(){
    return request({
        url:'/home/multidata'
    })
}

export function getHomeGoods(type, page){
    return request({
        url:'/home/data',
        params: {
            type,
            page,
        }
    })
}