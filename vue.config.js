const path = require('path');

module.exports = {
    configWebpack:{
        resolve:{
            //配置文件路径的别名
            alias:{
                assets: path.resolve(__dirname, './src/assets'),
                components: path.resolve(__dirname, './src/components'),
                network: path.resolve(__dirname, './src/network'),
                common: path.resolve(__dirname, './src/common'),
                views: path.resolve(__dirname, './src/views')
            }
        }
    }
}